﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudFlare.API.V4.Data
{
    public class CFOptions
    {
        public CFOptions(string apiKey, string email, string baseUrl, string zoneName = "", string record = "")
        {
            ApiKey = apiKey;
            Email = email;
            BaseUrl = baseUrl;
            ZoneName = zoneName;
            Record = record;
        }

        public string ApiKey { get; set; }

        public string Email { get; set; }

        public string BaseUrl { get; set; }

        public string ZoneName { get; set; }

        public string Record { get; set; }
    }
}
