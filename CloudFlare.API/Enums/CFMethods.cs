﻿namespace CloudFlare.API.Enums
{
    internal struct CFMethods
    {
        #region " Stats "
        
        public const string Stats = "stats";
        public const string LoadZones = "zone_load_multi";
        public const string LoadRecords = "rec_load_all";
        public const string ZoneCheck = "zone_check";
        public const string ZoneIps = "zone_ips";
        public const string IpThreatScore = "ip_lkup";
        public const string ZoneSettings = "zone_settings";

        #endregion

        #region " Stats "

        public const string AddRecord = "rec_new";
        public const string EditRecord = "rec_edit";
        public const string DeleteRecord = "rec_delete";

        #endregion

        #region " Modify "

        public const string SecurityLevel = "sec_lvl";
        public const string CacheLevel = "cache_lvl";
        public const string DevMode = "devmode";
        public const string PurgeCache = "fpurge_ts";
        public const string PurgeFile = "zone_file_purge";
        public const string ZoneGrab = "zone_grab";
        public const string SetIPV6 = "ipv46";
        public const string Async = "async";
        public const string Minify = "minify";
        public const string Mirage2 = "mirage2";

        #endregion
    }
}