﻿using CloudFlare.API.Utils;

namespace CloudFlare.API.Enums
{
    /// <summary>
    /// Result of the request
    /// </summary>
    public enum ResultKind
    {
        [StringValue("success")]
        Success,

        [StringValue("error")]
        Error
    }
}
