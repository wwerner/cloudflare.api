﻿namespace CloudFlare.API.V4.Enums
{
    /// <summary>
    /// Strings corresponding to the method's parameters
    /// </summary>
    /// 9/13/2013 by Sergi
    internal struct CFParameters
    {
        public const string Page = "page";
        public const string PageSize = "per_page";
        public const string Order = "order";
        public const string Direction = "direction";
        public const string Match = "match";
        public const string Name = "name";
    }
}
