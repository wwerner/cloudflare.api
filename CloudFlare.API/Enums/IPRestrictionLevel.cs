﻿using CloudFlare.API.Utils;

namespace CloudFlare.API.Enums
{
    /// <summary>
    /// Status for the developer mode
    /// </summary>
    /// 9/7/2013 by Sergi
    public enum IPRestrictionLevel
    {
        [StringValue("wl")]
        Whitelist = 0,

        [StringValue("ban")]
        Ban = 1,
        
        [StringValue("nul")]
        None = 2
    }
}
