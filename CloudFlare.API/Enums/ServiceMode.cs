﻿namespace CloudFlare.API.Enums
{
    /// <summary>
    /// Status of CloudFlare Proxy
    /// </summary>
    public enum ServiceMode
    {
        Off = 0,
        On = 1
    }
}
