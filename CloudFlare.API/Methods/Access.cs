﻿using System;
using System.Collections.Generic;
using System.Linq;
using CloudFlare.API.Data;
using CloudFlare.API.Enums;
using CloudFlare.API.Utils;
using RestSharp;

namespace CloudFlare.API.Methods
{
    /// <summary>
    /// Proxy class for Statistics related requests
    /// </summary>
    /// 9/5/2013 by Sergi
    public class Access
    {
        #region " Attributes "

        /// <summary>
        /// The request
        /// </summary>
        private static readonly RestRequest request = new RestRequest(Method.POST);

        #endregion " Attributes "

        #region " Public methods "

        /// <summary>
        /// Retrieve the current stats and settings for a particular website. 
        /// This function can be used to get currently settings of values such as the security level.
        /// http://www.cloudflare.com/docs/client-api.html#s3.1
        /// </summary>
        /// <param name="domain">The zone (domain) that statistics are being retrieved from.</param>
        /// <param name="interval">
        /// The time interval for the statistics denoted by these values: 
        /// For these values, the latest data is from one day ago
        ///     20 = Past 30 days
        ///     30 = Past 7 days
        ///     40 = Past day
        /// 
        /// The values are for Pro accounts
        ///     100 = 24 hours ago
        ///     110 = 12 hours ago
        ///     120 = 6 hours ago
        /// </param>
        /// <returns>A {StatsResume} object with the data</returns>
        /// 9/5/2013 by Sergi
        public StatsResume GetStats(string domain, Intervals interval = Intervals.Yesterday)
        {
            request.AddParameter(CFParameters.Method, CFMethods.Stats);
            request.AddParameter(CFParameters.Domain, domain);
            request.AddParameter(CFParameters.Interval, (int)interval);

            var response = CFProxy.Execute<StatsResponse>(request);
            return response.response.result;
        }

        /// <summary>
        /// This lists all domains in a CloudFlare account along with other data.
        /// http://www.cloudflare.com/docs/client-api.html#s3.2
        /// </summary>
        /// <returns>A List{Zones} with the domains retrieved</returns>
        /// 9/5/2013 by Sergi
        public Zones GetDomains()
        {
            request.AddParameter(CFParameters.Method, CFMethods.LoadZones);

            var response = CFProxy.Execute<DomainResponse>(request);
            return response.response.zones;
        }

        /// <summary>
        /// Lists all of the DNS records from a particular domain in a CloudFlare account.
        /// http://www.cloudflare.com/docs/client-api.html#s3.3
        /// </summary>
        /// <param name="domain">The domain that records are being retrieved from.</param>
        /// <returns>A List{DnsObject} with the list of records, or NULL if it was not successfull</returns>
        /// 9/5/2013 by Sergi
        public List<DnsObject> GetRecords(string domain)
        {
            request.AddParameter(CFParameters.Method, CFMethods.LoadRecords);
            request.AddParameter(CFParameters.Domain, domain);

            var response = CFProxy.Execute<DnsRecordsResponse>(request);
            return response.response.recs.objs;
        }

        /// <summary>
        /// Checks for active zones and returns their corresponding zids.
        /// http://www.cloudflare.com/docs/client-api.html#s3.4
        /// </summary>
        /// <param name="zones">List of zones separated by comma.</param>
        /// <returns>A {ZoneCheckResult} with the data</returns>
        /// 9/8/2013 by Sergi
        public ZoneCheckResult ZoneCheck(string zones)
        {
            request.AddParameter(CFParameters.Method, CFMethods.ZoneCheck);
            request.AddParameter(CFParameters.Zones, zones);

            var response = CFProxy.Execute<ZoneCheckResponse>(request);
            return response.response;
        }

        /// <summary>
        /// Returns a list of IP address which hit your site classified by type.
        /// http://www.cloudflare.com/docs/client-api.html#s3.5
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="hours">Past number of hours to query. Default is 24, maximum is 48.</param>
        /// <param name="ip_class">
        ///     Optional. Restrict the result set to a given class as given by:
        ///         "r" -- regular
        ///         "s" -- crawler
        ///         "t" -- threat
        /// </param>
        /// <param name="geo">Optional. Set to 1 to add longitude and latitude information to response</param>
        /// <returns>A List{IPHitsResult} with the data</returns>
        /// 9/8/2013 by Sergi
        public List<IPHitsResult> GetIPs(string domain, int hours = 24, IPClass ip_class = IPClass.All, int geo = 1)
        {
            request.AddParameter(CFParameters.Method, CFMethods.ZoneIps);
            request.AddParameter(CFParameters.Domain, domain);
            request.AddParameter(CFParameters.Hours, hours);
            request.AddParameter(CFParameters.Class, ip_class.GetStringValue());
            request.AddParameter(CFParameters.Geo, geo);

            var response = CFProxy.Execute<IPHitsResponse>(request);
            return response.response.ips;
        }

        /// <summary>
        /// Check threat score for a given IP.
        /// Find the current threat score for a given IP. Note that scores are on a logarithmic scale, 
        /// where a higher score indicates a higher threat.
        /// http://www.cloudflare.com/docs/client-api.html#s3.6
        /// </summary>
        /// <param name="ip">The ip.</param>
        /// <returns>The score threat value</returns>
        /// 9/8/2013 by Sergi
        public string GetIPThreatScore(string ip)
        {
            request.AddParameter(CFParameters.Method, CFMethods.IpThreatScore);
            request.AddParameter(CFParameters.IP, ip);

            var response = CFProxy.Execute<IPThreatResponse>(request);
            return response.response.ContainsKey(ip) ? response.response[ip] : String.Empty;
        }

        /// <summary>
        /// Gets all current setting values for a given domain.
        /// http://www.cloudflare.com/docs/client-api.html#s3.7
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <returns>A {ZoneSettings} object with the data</returns>
        /// 9/8/2013 by Sergi
        public ZoneSettings GetZoneSettings(string domain)
        {
            request.AddParameter(CFParameters.Method, CFMethods.ZoneSettings);
            request.AddParameter(CFParameters.Domain, domain);

            var response = CFProxy.Execute<ZoneSettingsResponse>(request);
            return response.response.result.objs.Count > 0 ? response.response.result.objs.First() : null;
        }

        #endregion " Public methods "
    }
}
