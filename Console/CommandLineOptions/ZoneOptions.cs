﻿using CommandLine;

namespace Console.CommandLineOptions
{
    public enum ZoneEnum
    {
        GetZones,
        GetZone
    }

    [Verb("zone", HelpText = "Get Zone information")]
    public class ZoneOptions : OptionsBase
    {
        [Option('c', "command", Required = true, HelpText = "Action to perform with the zone. Options are: 'GetZones', 'GetZone'", Default = ZoneEnum.GetZones)]
        public ZoneEnum Action { get; set; }

        [Option('z', "zoneName", Required = false, HelpText = "Zone name")]
        public string ZoneName { get; set; }

    }
}
